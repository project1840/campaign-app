import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import "./App.css";
import Home from "./page/Home";
import Layout from "./layouts/Layout";

function App() {
  return (
    <>
      <Router>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/campaignForm" element={<Layout />} />
        </Routes>
      </Router>
    </>
  );
}

export default App;
