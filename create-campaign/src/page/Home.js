function Home() {
  return (
    <>
      <div className="hm-bg">
        <img src="./assets/images/home_bg.svg" alt="" />
      </div>
      <div className="container-fluid">
        {/* <!-- home top --> */}
        <div className="hm-top d-flex justify-content-between pt-3">
          <div className="hm-logo-hldr d-flex align-items-center gap-3">
            <img src="./assets/images/cq5dam.web.1280.1280.png" alt="" />
            <span>AdVantage</span>
          </div>
          <div className="hm-top-link-hldr">
            <ul className="hm-top-link d-flex gap-3">
              <li>
                <a href="#url">Home</a>
              </li>
              <li>
                <a href="#url">About Us</a>
              </li>
            </ul>
          </div>
        </div>
        <div className="hm-content">
          <div className="hm-lh-text">
            <h1>Welcome to AdVantage</h1>
            <p>The next generation marketing assistant</p>
            <button className="cmn-btn cmn-rn-btn mx-auto">
              <p>Proceed</p>
              <div>
                <img src="" alt="" />
              </div>
            </button>
          </div>
        </div>
      </div>
    </>
  );
}

export default Home;
