import { useState } from "react";
import Form from "./Form";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import RemoveCircleOutlineRoundedIcon from "@mui/icons-material/RemoveCircleOutlineRounded";
import AddCircleOutlinedIcon from "@mui/icons-material/AddCircleOutlined";
import { Button, TextField } from "@mui/material";
import CustomButton from "../components/button/CustomButton";

function Campaign() {
  const [urls, setUrls] = useState([""]);
  const [next, setNext] = useState(false);

  const handleChange = (event, index) => {
    let urlSet = JSON.parse(JSON.stringify(urls));
    urlSet[index] = event.target.value;
    setUrls(urlSet);
  };

  const handleAdd = (index) => {
    let urlSet = JSON.parse(JSON.stringify(urls));
    urlSet.splice(index + 1, 0, "");
    console.log("add");
    setUrls(urlSet);
  };

  const handleRemove = (index) => {
    console.log("remove");
    let urlSet = JSON.parse(JSON.stringify(urls));
    urlSet.splice(index, 1);
    setUrls(urlSet);
  };

  const handleNext = () => {
    setNext(true);
  };

  return (
    <>
      {next ? (
        <Form setNext={setNext} next={next} urls={urls} />
      ) : (
        <>
          <div className="campaign-body-header">
            <p>Please enter the URL you want to use as baseline for content</p>
          </div>
          <div style={{ minHeight: "70vh" }}>
            {urls?.map((url, index) => {
              return (
                <div className="url-info">
                  <label required>Please Enter the Website URL</label>
                  <div className="url-input">
                    <TextField
                      id={`${url}-${index}`}
                      name={url}
                      label="Please Enter the Website URL"
                      onChange={(event) => handleChange(event, index)}
                      value={url}
                    />
                  </div>
                  <div className="url-action-btn">
                    {urls?.length > 1 && (
                      <Button
                        color="primary"
                        variant="outlined"
                        onClick={() => handleRemove(index)}
                        sx={{
                          backgroundColor: "#FBDBDB",
                          color: "#F65C5C",
                          display: "flex",
                          alignItems: "center",
                          gap: " 0.5rem",
                          border: "none",
                          ":hover": {
                            backgroundColor: "#FBDBDB",
                            border: "none",
                          },
                        }}
                      >
                        <RemoveCircleOutlineRoundedIcon
                          sx={{ fontSize: "1.2rem" }}
                        />
                        Remove
                      </Button>
                    )}
                    <Button
                      color="primary"
                      variant="outlined"
                      onClick={() => handleAdd(index)}
                      sx={{
                        backgroundColor: "#DBEBFB",
                        color: "#5AACFF",
                        display: "flex",
                        alignItems: "center",
                        gap: " 0.5rem",
                        border: "none",
                        ":hover": {
                          backgroundColor: "#DBEBFB",
                          border: "none",
                        },
                      }}
                    >
                      <AddCircleOutlinedIcon sx={{ fontSize: "1.2rem" }} />
                      Add Website
                    </Button>
                  </div>
                </div>
              );
            })}
          </div>
          <CustomButton
            nextClick={handleNext}
            isBack={false}
            isNext={true}
            isSubmit={false}
            rightText={"Next"}
          />
        </>
      )}
    </>
  );
}

export default Campaign;
