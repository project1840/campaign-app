import { useState } from "react";
import CreateCampaign from "../components/forms/CreateCampaign";
import ConfigureCampaign from "../components/forms/ConfigureCampaign";
import AddOffers from "../components/forms/AddOffers";
import Overview from "../components/forms/Overview";
import Action from "../components/forms/Action";
import { Box, Stepper, Step, StepLabel, Stack } from "@mui/material";

function Form({ setNext, next, urls, setUrls }) {
  const formSchema = {
    campaignName: "",
    campaignType: "",
    targetLanguage: "",
    campaignProposition: "",
    offerName: "",
    product: "",
    existingRatePrice: "",
    offeredRatePrice: [],
    information: "",
    persona: [],
  };
  const [snapshot, setSnapshot] = useState(formSchema);
  const [activeStep, setActiveStep] = useState(0);
  const steps = [
    "Create Campaign",
    "Configure Campaign",
    "Add Offers",
    "Overview",
    "Sent It or Save It",
  ];
  const isLastStep = Boolean(steps.length === activeStep + 1);

  const handleNextStep = (newData, isNext = true) => {
    setSnapshot((prevData) => ({ ...prevData, ...newData }));
    if (isNext) {
      setActiveStep((prevStep) => prevStep + 1);
    }
  };

  const handlePreviousStep = (newData) => {
    setSnapshot((prevData) => ({ ...prevData, ...newData }));
    if (next) setActiveStep((prevStep) => prevStep - 1);
  };

  const formComponents = [
    <CreateCampaign
      key={1}
      data={snapshot}
      next={handleNextStep}
      previous={handlePreviousStep}
      setNext={setNext}
      urls={urls}
      setUrls={setUrls}
    />,
    <ConfigureCampaign
      key={2}
      data={snapshot}
      next={handleNextStep}
      previous={handlePreviousStep}
      urls={urls}
      setUrls={setUrls}
      snapshot={snapshot}
      setSnapshot={setSnapshot}
    />,
    <AddOffers
      key={3}
      data={snapshot}
      next={handleNextStep}
      previous={handlePreviousStep}
    />,
    <Overview
      key={4}
      data={snapshot}
      next={handleNextStep}
      previous={handlePreviousStep}
    />,
    <Action
      key={5}
      data={snapshot}
      previous={handlePreviousStep}
      isLastStep={isLastStep}
    />,
  ];

  return (
    <Box
      sx={{
        display: "block",
        //  maxWidth: "90rem", margin: "0 auto"
      }}
    >
      <Stack
      // sx={{ width: "100%" }} spacing={4}
      >
        <Stepper
          activeStep={activeStep}
          alternativeLabel
          //  connector={<CustomTheme.CustomStepConnector />}
        >
          {steps?.map((currentStep) => {
            const label = currentStep;
            return (
              <Step key={label}>
                <StepLabel
                //  StepIconComponent={CustomTheme.StepIcon}
                >
                  {label}
                </StepLabel>
              </Step>
            );
          })}
        </Stepper>
        {formComponents[activeStep]}
      </Stack>
    </Box>
  );
}

export default Form;
