// import TextField from "@mui/material/TextField";

// export default function CustomTextField({ formik, name, label }) {
//   return (
//     <div
//       style={{
//         display: "flex",
//         flexDirection: "column",
//       }}
//     >
//       <label>{label}</label>

//       <TextField
//        sx={{
//         maxWidth: "15rem",
//       }}
//         id={name}
//         name={name}
//         label={label}
//         onChange={formik.handleChange}
//         onBlur={formik.handleBlur}
//         value={formik.values[name]}
//       />
//     </div>
//   );
// }
import TextField from "@mui/material/TextField";

export default function CustomTextField({ formik, name, label }) {
  return (
    <div className="d-flex flex-column mb-3">
      <label for="exampleInputEmail1" className="form-label">
        {label}
      </label>

      <TextField
        sx={{
          maxWidth: "15rem",
        }}
        id={name}
        name={name}
        label={label}
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        value={formik.values[name]}
        className="form-control"
      />
    </div>
  );
}
