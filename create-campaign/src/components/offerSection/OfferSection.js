function OfferSection({ offers, setOffers }) {
  const handleActiveOffer = (index) => {
    let offerSet = JSON.parse(JSON.stringify(offers));
    offerSet[index].checked = !offerSet[index]?.checked;
    setOffers(offerSet);
  };
  return (
    <div className="offer-header">
      <p>From your Website</p>
      <div className="offer-container">
        {offers?.map((element, index) => {
          return (
            <div className="offer" key={`${element?.offer}-${index}`}>
              {element?.offer}
              <input
                type="button"
                className={`${
                  element?.checked ? "active-offer" : "inactive-offer"
                }`}
                onClick={() => handleActiveOffer(index)}
              />
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default OfferSection;
