import LanguageIcon from "@mui/icons-material/Language";
import CancelRoundedIcon from "@mui/icons-material/CancelRounded";

function UrlSection({ urls, setUrls }) {
  const handleUrlDelete = (index) => {
    let urlSet = JSON.parse(JSON.stringify(urls));
    urlSet.splice(index, 1);
    setUrls(urlSet);
  };
  return (
    <div className="url-container">
      {urls?.map((url, index) => {
        if (url !== "") {
          return (
            <div className="url" key={`${url}-${index}`}>
              <LanguageIcon />
              {url}
              <CancelRoundedIcon
                sx={{
                  fill: "#BABABA",
                  fontSize: "1.2rem",
                }}
              />
            </div>
          );
        }
      })}
    </div>
  );
}

export default UrlSection;
