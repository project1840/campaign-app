import { Button } from "@mui/material";
import FormNavigation from "./FormNavigation";
import LocalPrintshopOutlinedIcon from "@mui/icons-material/LocalPrintshopOutlined";
import FileDownloadOutlinedIcon from "@mui/icons-material/FileDownloadOutlined";
import salesforce from "../../assets/images/Icon awesome-salesforce.png";
import feathersend from "../../assets/images/Icon feather-send.png";
import dynamicImage from "../../assets/images/Microsoft_Dynamics_Logo.png";
import overview from "../../assets/images/Group 4118.png";

function Action({ data, previous }) {
  const buttonStyle = {
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center",
    gap: "0.8rem",
    fontSize: "1.4rem",
    color: "black",
    // width: 7rem;
    border: "1px solid #0A7DB7",
    paddingLeft: " 0.5rem",
    borderRadius: ".5rem",
    width: "7rem",
  };
  return (
    <div>
      <div className="campaign-Action">
        <div className="action-image">
          <img src={overview} />
        </div>
        <div className="actions">
          <Button sx={buttonStyle}>
            <img src={feathersend} />
            Send
          </Button>
          <Button sx={buttonStyle}>
            <LocalPrintshopOutlinedIcon />
            Print
          </Button>
          <Button sx={{ ...buttonStyle, width: "10.5rem" }}>
            <FileDownloadOutlinedIcon />
            Download
          </Button>
          <Button sx={{ ...buttonStyle, width: "16.5rem" }}>
            <img src={salesforce} />
            Add to Salesforce
          </Button>
          <Button sx={{ ...buttonStyle, width: "18rem" }}>
            <img src={dynamicImage} />
            Add to Dynamics 365
          </Button>
        </div>
      </div>
      <div>
        <FormNavigation
          // disabled={true}
          isLastStep
          formik={{ values: data }}
          previous={previous}
          leftText={"Back"}
        />
      </div>
    </div>
  );
}

export default Action;
