import * as React from "react";
import Button from "@mui/material/Button";
import CustomButton from "../button/CustomButton";

const FormNavigation = ({
  disabled = false,
  isLastStep = false,
  formik,
  previous,
  setNext = false,
  leftText = "",
  rightText = "",
}) => {
  const handleBack = () => {
    if (setNext) {
      setNext(false);
    }
    previous(formik.valuess);
  };
  const props = {
    isBack: true,
    prevClick: () => handleBack(),
    leftText,
    rightText,
  };
  if (!isLastStep) {
    props.isNext = true;
    props.isSubmit = true;
  }

  return <CustomButton {...props} />;
};
export default FormNavigation;
