import { Formik, Form } from "formik";
import FormNavigation from "./FormNavigation";
import CustomTextField from "../textField/CustomTextField";
import UrlSection from "../urls/UrlSection";

function CreateCampaign({ data, next, previous, setNext, setUrls, urls }) {
  const handleSubmit = (values) => {
    next(values);
  };
  return (
    <Formik initialValues={data} onSubmit={handleSubmit}>
      {(formik) => (
        <Form>
          {urls?.length > 0 && <UrlSection setUrls={setUrls} urls={urls} />}
          <div>
            <div className="field-box">
              <CustomTextField
                formik={formik}
                name="campaignName"
                label="Please Enter Campaign Name"
              />
            </div>
            <div className="row field-box">
              <div className="col-6">
                <CustomTextField
                  formik={formik}
                  name="campaignType"
                  label="Please Select Campaign Type"
                />
              </div>

              <div className="col-6" >
                <CustomTextField
                  formik={formik}
                  name="targetLanguage"
                  label="Please Enter Target Language"
                />
              </div>
            </div>

            <div className="field-box">
              <CustomTextField
                formik={formik}
                name="campaignProposition"
                label="Please Enter Campaign Proposition"
              />
            </div>
          </div>

          <FormNavigation
            // disabled={true}
            formik={formik}
            isLastStep={false}
            previous={previous}
            setNext={setNext}
            leftText={"Back"}
            rightText={"Next"}
          />
        </Form>
      )}
    </Formik>
  );
}

export default CreateCampaign;
