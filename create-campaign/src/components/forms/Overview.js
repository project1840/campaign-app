import { useEffect, useState } from "react";
import { Formik, Form } from "formik";
import { Button, CircularProgress } from "@mui/material";
import FormNavigation from "./FormNavigation";
import overview from "../../assets/images/Group 4118.png";
import featheredit from "../../assets/images/Icon feather-edit-3.png";
import refresh from "../../assets/images/refresh.png";
function Overview({ data, next, previous }) {
  const [isLoading, setLoading] = useState(false);
  const buttonStyle = {
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center",
    gap: "0.8rem",
    fontSize: "1.2rem",
    // border: "1px solid",
    padding: " 0rem 1.5rem",
    // color: "red",
  };
  const handleSubmit = (values) => {
    next(values);
  };
  useEffect(() => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
    },3000);
  }, []);
  return (
    <Formik initialValues={data} onSubmit={handleSubmit}>
      {(formik) => (
        <Form>
          {isLoading ? (
            <div className="loader">
              <CircularProgress />
            </div>
          ) : (
            <div className="campaign-overview">
              <div className="overview-image">
                <img src={overview} />
              </div>
              <div className="overview-action">
                <Button
                  sx={{
                    ...buttonStyle,
                    border: "1px solid #AF0000",
                    color: "#AF0000",
                  }}
                >
                  <img src={featheredit} />
                  Edit
                </Button>
                <Button
                  sx={{
                    ...buttonStyle,
                    border: "1px solid #0A7DB7",
                    color: "#0A7DB7",
                  }}
                >
                  <img src={refresh} />
                  Re-Generate
                </Button>
              </div>
            </div>
          )}
          <FormNavigation
            // disabled={true}
            isLastStep={false}
            formik={{ values: data }}
            previous={previous}
            leftText={"Back"}
            rightText={"Next"}
          />
        </Form>
      )}
    </Formik>
  );
}

export default Overview;
