import { useState } from "react";
import { Formik, Form } from "formik";
import FormNavigation from "./FormNavigation";
import UrlSection from "../urls/UrlSection";
import VisibilityIcon from "@mui/icons-material/Visibility";
import AddCircleOutlinedIcon from "@mui/icons-material/AddCircleOutlined";
import featheredit from "../../assets/images/Icon feather-edit-3.png";
import deleteImage from "../../assets/images/Icon material-delete.png";
import Modal from "../modal/Modal";
import { Button } from "@mui/material";

const buttonStyle = {
  display: "flex",
  justifyContent: "flex-start",
  alignItems: "center",
  gap: "0.8rem",
  fontSize: "1.2rem",
  padding: " 0rem 1.5rem",
};

function ConfigureCampaign({
  data,
  next,
  previous,
  setUrls,
  urls,
  snapshot,
  setSnapshot,
}) {
  const [open, setOpen] = useState(false);
  const [action, setAction] = useState(null);
  const [recordManupulationId, setRecordManupulationId] = useState(null);

  const handleSubmit = (values) => {
    next(values);
  };
  const handleAddNewPersona = (event) => {
    event.stopPropagation();
    setRecordManupulationId(null);
    setAction("ADD");
    setOpen(true);
  };
  const handleViewPersona = (id) => {
    setRecordManupulationId(id);
    setAction("VIEW");
    setOpen(true);
  };
  const handleEditPersona = (id) => {
    setRecordManupulationId(id);
    setAction("EDIT");
    setOpen(true);
  };
  const handleDeletePersona = (id) => {
    console.log("comming to delete!!!!!! yesssssssssss");
    setRecordManupulationId(id);
    setAction("DELETE");
    let newFormikData = JSON.parse(JSON.stringify(snapshot));
    console.log(newFormikData.persona, " form data before deletion");
    newFormikData.persona.splice(id, 1);
    console.log(newFormikData.persona, " form data after deletion");
    setSnapshot(newFormikData);
  };
  const getCreatePersonaBtn = () => {
    return (
      <button
        type="button"
        className="create-persona-btn"
        onClick={(event) => handleAddNewPersona(event)}
      >
        <AddCircleOutlinedIcon
          sx={{ fontSize: "2.6rem" }}
          onClick={(event) => handleAddNewPersona(event)}
        />
        Create Persona
      </button>
    );
  };
  return (
    <Formik initialValues={data} onSubmit={handleSubmit}>
      {(formik) => (
        <Form>
          {open && (
            <Modal
              open={open}
              setOpen={setOpen}
              formik={{ values: snapshot }}
              next={next}
              recordManupulationId={recordManupulationId}
              setRecordManupulationId={setRecordManupulationId}
              action={action}
              setAction={setAction}
            />
          )}
          <div className="url-persona-contrainer">
            {urls.length > 0 && <UrlSection setUrls={setUrls} urls={urls} />}
            {snapshot?.persona?.length > 0 && <>{getCreatePersonaBtn()}</>}
          </div>
          {snapshot?.persona?.length === 0 ? (
            // <div className="btn-persona-container">
            //   <button
            //     type="button"
            //     className="create-persona-btn"
            //     onClick={(event) => handleAddNewPersona(event)}
            //   >
            //     <AddCircleOutlinedIcon
            //       sx={{ fontSize: "2.6rem" }}
            //       onClick={(event) => handleAddNewPersona(event)}
            //     />
            //     Create Persona
            //   </button>
            // </div>
            <div className="btn-persona-container">{getCreatePersonaBtn()}</div>
          ) : (
            <div>
              {snapshot?.persona?.map((person, id) => {
                return (
                  <div className="in-pdt-hldr d-flex active">
                    <div className="in-img-hldr">
                      <img src={person?.profileImage} alt="" />
                    </div>
                    <div className="d-flex flex-column">
                      <h2>{person?.personalDetails?.name}</h2>
                      <div className="d-flex _justify-content-between gap-5">
                        <div>
                          <p>
                            Age: <strong>{person?.personalDetails?.age}</strong>
                          </p>
                          <p>
                            Occupation:{" "}
                            <strong>
                              {person?.personalDetails?.occupation}
                            </strong>
                          </p>
                          <p>
                            Field of Work:{" "}
                            <strong>
                              {person?.personalDetails?.fieldOfWork}
                            </strong>
                          </p>
                        </div>
                        <div>
                          <p>
                            Martial Status:{" "}
                            <strong>
                              {person?.personalDetails?.martialStatus}
                            </strong>
                          </p>
                          <p>
                            Location:{" "}
                            <strong>{person?.personalDetails?.location}</strong>
                          </p>
                          <p>
                            Financial Status:{" "}
                            <strong>
                              {person?.personalDetails?.financialStatus}
                            </strong>
                          </p>
                        </div>
                        <div className="d-flex gap-1 in-btn-hldr">
                          <Button
                            sx={{
                              ...buttonStyle,
                              border: "1px solid #0A7DB7",
                              color: "white",
                              backgroundColor: "#0A7DB7",
                              display: "flex",
                              alignItems: "center",
                              gap: " 0.5rem",
                              border: "none",
                              ":hover": {
                                color: "white",
                                backgroundColor: "#0A7DB7",
                                border: "none",
                              },
                            }}
                            onClick={() => handleViewPersona(id)}
                          >
                            <VisibilityIcon /> View Details
                          </Button>
                          <Button
                            sx={{
                              ...buttonStyle,
                              border: "1px solid black",
                              color: "black",
                              borderStyle: "dashed",
                            }}
                            onClick={() => handleEditPersona(id)}
                          >
                            <img src={featheredit} /> Edit Details
                          </Button>
                          <Button
                            sx={{
                              ...buttonStyle,
                              border: "1px solid #AF0000",
                              color: "#AF0000",
                              border: "none",
                            }}
                            onClick={() => handleDeletePersona(id)}
                          >
                            <img src={deleteImage} alt="" /> Delete
                          </Button>
                        </div>
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          )}

          <FormNavigation
            // disabled={true}
            formik={formik}
            isLastStep={false}
            previous={previous}
            leftText={"Back"}
            rightText={"Next"}
          />
        </Form>
      )}
    </Formik>
  );
}

export default ConfigureCampaign;
