import { Formik, Form } from "formik";
import FormNavigation from "./FormNavigation";
import CustomTextField from "../textField/CustomTextField";
import { useState } from "react";
import OfferSection from "../offerSection/OfferSection";
function AddOffers({ data, next, previous }) {
  const [offers, setOffers] = useState([
    {
      offer: "Interest Rate Discount Of 0.15% For Label A Homes",
      checked: false,
    },
    { offer: "€25 Cashbackwith A New Credit Card" },
    {
      offer: "€50 Cashback With 2 Or More Home Insurance Policies",
      checked: false,
    },
    { offer: "3.8% Fixed Rate For 15 Year Mortgages", checked: false },
    { offer: "3000 Bonus Points On Amex Card", checked: false },
    {
      offer: "10% Discount On Your Premium Of Car Insurance Policy",
      checked: false,
    },
  ]);
  const handleSubmit = (values) => {
    next(values);
  };
  return (
    <Formik initialValues={data} onSubmit={handleSubmit}>
      {(formik) => (
        <Form>
          <OfferSection offers={offers} setOffers={setOffers} />
          <div>
            <div className="row">
              <div className="col-6">
                <CustomTextField
                  formik={formik}
                  name="offerName"
                  label="Please Enter Offer Name"
                />
              </div>

              <div className="col-6">
                <CustomTextField
                  formik={formik}
                  name="product"
                  label="Please Select Product"
                />
              </div>
            </div>
            <div className="row">
              <div className="col-6">
                <CustomTextField
                  formik={formik}
                  name="existingRatePrice"
                  label="Please Enter Existing Rate/Price"
                />
              </div>

              <div className="col-6">
                <CustomTextField
                  formik={formik}
                  name="offeredRatePrice"
                  label="Please Enter Offered Rate/Price"
                />
              </div>
            </div>
            <div>
              <CustomTextField
                formik={formik}
                name="information"
                label="Please Enter More Information (If any)"
              />
            </div>
          </div>

          <FormNavigation
            // disabled={true}
            formik={formik}
            isLastStep={false}
            previous={previous}
            leftText={"Back"}
            rightText={"Generate"}
          />
        </Form>
      )}
    </Formik>
  );
}

export default AddOffers;
