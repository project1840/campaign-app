import { Modal, Box } from "@mui/material";
import { Formik, Form } from "formik";
import fileUpload from "../../assets/images/Icon awesome-file-upload.png";
import "./Modal.css";
import { useState } from "react";
import CustomButton from "../button/CustomButton";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",

  backgroundColor: "white",
  border: "2px solid #000",
  boxShadow: 24,
  width: "70%",
  padding: "2rem",
};

const personalDetails = [
  "Age",
  "Occupation",
  "Field of Work",
  "Martial Status",
  "Location",
  "Financial Status",
];

let InfoLabelAsPerFormik = [
  "age",
  "occupation",
  "fieldOfWork",
  "martialStatus",
  "location",
  "financialStatus",
];

let personaObject = {
  profileImage: "",
  personalDetails: {
    name: "",
    age: "",
    occupation: "",
    fieldOfWork: "",
    martialStatus: "",
    location: "",
    financialStatus: "",
  },
  background: ["", "", ""],
  context: "",
  goals: ["", "", "", ""],
  motivations: ["", "", ""],
  frustrations: ["", "", ""],
  technologyAndSocialMedia: "",
};

function PersonaModal({
  open,
  setOpen,
  formik,
  next,
  recordManupulationId,
  setRecordManupulationId,
  action,
  setAction,
}) {
  const [persona, setPersona] = useState(
    recordManupulationId !== null
      ? formik?.values?.persona[recordManupulationId]
      : personaObject
  );
  console.log(recordManupulationId, "add new", persona);
  const handleClose = () => {
    setAction(null);
    setRecordManupulationId(null);
    setOpen(false);
  };

  const handleBack = () => {
    handleClose();
  };

  const handleFileChange = (event) => {
    let file = event.target.files[0];
    let newPersona = JSON.parse(JSON.stringify(persona));
    newPersona.profileImage = URL.createObjectURL(file);
    setPersona(newPersona);
  };

  const handlePersonalData = (event, field) => {
    let newPersona = JSON.parse(JSON.stringify(persona));
    newPersona.personalDetails[field] = event.target.value;
    setPersona(newPersona);
  };

  const handleNonPersonalData = (event, field, id = null) => {
    let newPersona = JSON.parse(JSON.stringify(persona));
    if (id === null) {
      newPersona[field] = event.target.value;
    } else {
      newPersona[field][id] = event.target.value;
    }
    setPersona(newPersona);
  };
  const handleSubmit = () => {
    let newData;
    if (recordManupulationId === null) {
      formik.values.persona.push(persona);
    } else {
      formik.values.persona[recordManupulationId] = persona;
    }
    newData = formik;
    next(newData.values, false);
    handleClose();
  };
  const props = {
    isBack: true,
    prevClick: () => handleBack(),
    leftText: "Cancel",
    rightText: "Save",
    isNext: true,
    isSubmit: false,
    nextClick: () => handleSubmit(),
  };
  return (
    <Modal
      open={open}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
      sx={{ overflow: "scroll", height: "100%" }}
    >
      <Box sx={style}>
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            gap: "2rem",
          }}
        >
          <div className="container-one">
            <div className="basic-details">
              <div className="photo">
                <img src={fileUpload} />
                <div>
                  <label>Upload Picture</label>
                  <input
                    type="file"
                    name="file"
                    onChange={handleFileChange}
                  />
                </div>
              </div>
              <div className="personal-details">
                <input
                  placeholder="Enter Name"
                  type="text"
                  value={
                    // formik?.values?.persona[0]?.personalDetails["name"]
                    persona?.personalDetails?.name
                  }
                  onChange={(event) => {
                    if (action !== "VIEW")
                      return handlePersonalData(event, "name");
                  }}
                />
                {personalDetails?.map((item, index) => {
                  let field = InfoLabelAsPerFormik[index];
                  return (
                    <div className="input-field" key={`${item}-${index}`}>
                      <label>{`${item}: `} </label>
                      <input
                        type="text"
                        value={
                          // formik?.values?.persona[0]?.personalDetails[
                          //   field
                          // ]
                          persona?.personalDetails[field]
                        }
                        onChange={(event) => {
                          if (action !== "VIEW")
                            return handlePersonalData(event, field);
                        }}
                      />
                    </div>
                  );
                })}
              </div>
            </div>

            <div className="background">
              <h3>Background</h3>
              {persona?.background.length &&
                persona?.background?.map((item, id) => {
                  return (
                    <div>
                      <span className="dot"></span>{" "}
                      <input
                        type="text"
                        value={persona?.background[id]}
                        onChange={(event) => {
                          if (action !== "VIEW")
                            return handleNonPersonalData(
                              event,
                              "background",
                              id
                            );
                        }}
                      />
                    </div>
                  );
                })}
            </div>
            <div className="context">
              <h3>Context</h3>
              <textarea
                value={persona?.context}
                rows="3"
                onChange={(event) => {
                  if (action !== "VIEW")
                    return handleNonPersonalData(event, "context");
                }}
              />
            </div>
          </div>
          <div className="container-two">
            <div className="goals">
              <h3>Goals</h3>
              {persona?.goals.length &&
                persona?.goals.map((item, id) => {
                  return (
                    <div>
                      <span className="dot"></span>{" "}
                      <input
                        type="text"
                        value={persona?.goals[id]}
                        onChange={(event) => {
                          if (action !== "VIEW")
                            return handleNonPersonalData(event, "goals", id);
                        }}
                      />
                    </div>
                  );
                })}
            </div>
            <div className="motivations">
              <h3>Motivations</h3>
              {persona?.motivations?.map((item, id) => {
                return (
                  <div>
                    <span className="dot"></span>{" "}
                    <input
                      type="text"
                      value={persona?.motivations[id]}
                      onChange={(event) => {
                        if (action !== "VIEW")
                          return handleNonPersonalData(
                            event,
                            "motivations",
                            id
                          );
                      }}
                    />
                  </div>
                );
              })}
            </div>
            <div className="frustrations">
              <h3>Frustrations</h3>
              {persona?.frustrations?.map((item, id) => {
                return (
                  <div>
                    <span className="dot"></span>{" "}
                    <input
                      type="text"
                      value={persona?.frustrations[id]}
                      onChange={(event) => {
                        if (action !== "VIEW")
                          return handleNonPersonalData(
                            event,
                            "frustrations",
                            id
                          );
                      }}
                    />
                  </div>
                );
              })}
            </div>
            <div className="social">
              <h3>Technology & social media</h3>
              <textarea
                value={persona?.technologyAndSocialMedia}
                rows="3"
                onChange={(event) => {
                  if (action !== "VIEW")
                    return handleNonPersonalData(
                      event,
                      "technologyAndSocialMedia"
                    );
                }}
              />
            </div>
          </div>
        </div>
        <div style={{ padding: "2rem" }}>
          <CustomButton {...props} />
        </div>
      </Box>
    </Modal>
  );
}

export default PersonaModal;
