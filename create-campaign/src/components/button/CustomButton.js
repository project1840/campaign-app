import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import { Button } from "@mui/material";

function CustomButton({
  nextClick = false,
  isBack,
  isNext,
  prevClick = false,
  isSubmit = true,
  leftText = "",
  rightText = "",
}) {
  const nextProps = {
    variant: "outlined",
  };
  if (isSubmit) {
    nextProps.type = "submit";
  } else {
    nextProps.onClick = () => {
      return nextClick();
    };
  }
  return (
    <div
      className="custom-btn"
    //   style={{
    //     display: "flex",
    //     justifyContent: `${
    //       isBack && isNext
    //         ? "space-between"
    //         : isNext
    //         ? "flex-end"
    //         : "flex-start"
    //     }`,
    //     width: "96%",
    //     marginTop: "3rem",
    //   }}
    >
      {isBack && (
        <Button
          variant="outlined"
          onClick={prevClick}
          sx={{
            position: "relative",
            backgroundColor: "unset !important",
            color: "#BABABA !important",
            fontSize: "1.5rem",
            fontWeight: "bold",
            padding: `${
              leftText === "Cancel" ? "0.4rem 1.5rem" : "0.4rem 2.2rem"
            }`,

            border: "1px solid #707070 !important",
            borderRadius: "15px",
            float: "left",
          }}
        >
          {leftText}
          <ArrowBackIcon
            sx={{
              position: "absolute",
              fontSize: "4rem",
              right: "6.8rem",
              background: "#BABABA",
              color: "white",
              borderRadius: "50%",
            }}
          />
        </Button>
      )}
      {isNext && (
        <Button
          {...nextProps}
          sx={{
            position: "relative",
            backgroundColor: "unset !important",
            color: "black !important",
            fontSize: "1.5rem",
            fontWeight: "bold",
            padding: "0.4rem 2.2rem",
            border: "1px solid #707070 !important",
            borderRadius: "15px",
            float: "right",
          }}
        >
          {rightText}
          <ArrowForwardIcon
            sx={{
              position: "absolute",
              fontSize: "4rem",
              right: "-2.7rem",
              background: "#0A7DB7",
              color: "white",
              borderRadius: "50%",
            }}
          />
        </Button>
      )}
    </div>
  );
}

export default CustomButton;
