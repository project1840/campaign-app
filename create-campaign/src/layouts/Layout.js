import Campaign from "../page/Campaign";
import Image1 from "../assets/images/cq5dam.web.1280.1280.png";
import Image2 from "../assets/images/cq5dam.web.1280.1280.png";

function Layout() {
  return (
    <div className="in-main d-flex ">
      {/* <!-- left section --> */}
      <section className="in-lp p-3">
        <div className="in-logo-hldr d-flex align-items-center gap-3">
          <img src={Image1} alt="" className="me-2" />
          <div>AdVantage</div>
        </div>

        <div className="in-lnav-hldr">
          <ul className="w-100 p-0 in-lnav-nav">
            <li>
              <a href="#url" className="active">
                <img src={Image2} alt="" /> Campaign
              </a>
            </li>
            <li>
              <a href="#url">
                <img src={Image2} alt="" /> History
              </a>
            </li>
          </ul>
        </div>
      </section>

      {/* <!-- right section --> */}

      <section className="d-flex flex-column h-100 in-rp pt-3 px-5">
        <main className="flex-shrink-0">
          <div className="in-ts">
            <h1 className="in-h1">Create a Campaign</h1>
            <p>Generate a AI based marketing campaign</p>
          </div>
          <Campaign />
        </main>

        {/* <footer className="in-fh mt-auto py-3">
          <span className="text-muted">Place sticky footer content here.</span>
        </footer> */}
      </section>
    </div>
  );
}

export default Layout;
